public class App {
    public static void main(String[] args) throws Exception {
        // đối tượng Animal
        Animal animal1 = new Animal("HoaDa");
        Animal animal2 = new Animal("QuachTinh");
        System.out.println("====Animal===");
        System.out.println(animal1);
        System.out.println(animal2);


        // đối tượng mammal
        Mammal mammal1 = new Mammal("DuongQua");
        Mammal mammal2 = new Mammal("CoCo");
        System.out.println("====mammal===");
        System.out.println(mammal1);
        System.out.println(mammal2);

            // đối tượng cat
        Cat cat1 = new Cat("lola");
        Cat cat2 = new Cat("pipo");
        System.out.println("====Cat===");
        System.out.println(cat1);
        System.out.println(cat2);

            // đối tượng dog
            Dog dog1 = new Dog("Muc");
            Dog dog2 = new Dog("Bong");
            System.out.println("====dog===");
            System.out.println(dog1);
            System.out.println(dog2);

            // tiếng mèo kêu
            System.out.println("tieng meo keu");
            cat1.greets();
            cat2.greets();

            // tieng cho sua
            System.out.println("tieng cho sua");
            dog1.greets();
            dog2.greets();
            dog1.greets(dog2);

    }
}
